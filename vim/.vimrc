syntax on

map j gj
map k gk
"noremap <Up> <Nop>
"noremap <Down> <Nop>
"noremap <Left> <Nop>
"noremap <Right> <Nop>
"noremap <End> <Nop>
"noremap <Home> <Nop>
"noremap <PageUp> <Nop>
"noremap <PageDown> <Nop>

let &t_SI = "\e[6 q"
let &t_EI = "\e[2 q"

" optional reset cursor on start:
augroup myCmds
au!
autocmd VimEnter * silent !echo -ne "\e[2 q"
augroup END

set mouse=a
set ttymouse=sgr
map <ScrollWheelUp>         3kzz
map <ScrollWheelDown>       3jzz
map <ScrollWheelLeft>       3h
map <ScrollWheelRight>      3l
map <C-ScrollWheelUp>       21kzz
map <C-ScrollWheelDown>     21jzz
map <C-ScrollWheelLeft>     21h
map <C-ScrollWheelRight>    21l
map <A-ScrollWheelUp>       kzz
map <A-ScrollWheelDown>     jzz
map <A-ScrollWheelLeft>     h
map <A-ScrollWheelRight>    l

set completeopt=longest,noselect
set complete=.,w,b,u,t,i,kspell
set infercase

set clipboard=unnamed

set modeline
set tabstop=8
set expandtab
set softtabstop=4
set shiftwidth=4

set ruler
"set number
"set lines=999

hi ColorColumn ctermbg=grey guibg=lightgrey
set colorcolumn=+2
set background=dark

filetype plugin on
filetype indent on

set foldmethod=manual

vnoremap <C-n> :norm i
function! Latex()
    execute "w"
    execute "! xelatex %"
endfunc
command -nargs=0 Latex call Latex()
nnoremap <Leader>l :Latex <CR>
function! BiberLatex()
    execute "w"
    execute "! xelatex % && biber %:r && xelatex %"
endfunc
command -nargs=0 BiberLatex call BiberLatex()
nnoremap <Leader>b :BiberLatex <CR>
function! Python()
    execute "w"
    execute "! python %"
endfunc
command -nargs=0 Python call Python()
nnoremap <Leader>p :Python <CR>
function! GitCommit()
    execute "w"
    execute "! git commit % && git pull && git push"
endfunc
command -nargs=0 GitCommit call GitCommit()
nnoremap <Leader>g :GitCommit <CR>

"set guioptions -=m 
"set guioptions -=T
"set guioptions -=r

let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_math = 1
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_strikethrough = 1
